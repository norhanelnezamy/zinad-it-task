# Interview Tasks

1- Create a report of a tree of tasks.
2- Create a report for users courses and their attendance

## Installation and configuration steps

```bash
git clone https://gitlab.com/norhanelnezamy/zinad-it-task.git
```
```bash
composer update
```
```bash
php artisan key:generate
```
please create a database in your SQL serve

```bash
cp -i .env.example .env
```
configure your database connection

```bash
php artisan migrate
```
```bash
php artisan db:seed
```
```bash
php artisan serve
```

## Usage

Use [users-courses reports](http://127.0.0.1:8000/users-courses/report) to get a report of users courses attendance.
Use [tasks reports](http://127.0.0.1:8000/tasks/report) to get a tree of reports.
