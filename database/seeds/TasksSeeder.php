<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TasksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $tasks = [];
      $tasks_status = ['pending', 'done'];
      for ($i=1; $i <=  100 ; $i++) {
        $empty_and_random_id = [null, rand(1, $i)];
        $task = [
          'title' => "task $i",
          'parent_id' => $empty_and_random_id[rand(0, 1)],
          'status' => $tasks_status[rand(0, 1)],
        ];
        array_push($tasks, $task);
      }
      DB::table('tasks')->insert($tasks);
    }
}
