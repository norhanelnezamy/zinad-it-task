<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersCoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $users_courses = [];
      for ($i=1; $i <=  20 ; $i++) {
        for ($j=1; $j <= 5 ; $j++) {
          $attended_lessons = [];
          $attended_lessons_length = rand(1,9);
          for ($k=1; $k <= $attended_lessons_length; $k++) {
            array_push($attended_lessons, ['lesson_id' => $k,'date' => date('Y-m-d H:i:s')]);
          }
          $user_course_data = ['course_id' => $j, 'user_id' => $i, 'attendance' => json_encode($attended_lessons)];
          array_push($users_courses, $user_course_data);
        }
      }
      DB::table('users_courses')->insert($users_courses);
    }
}
