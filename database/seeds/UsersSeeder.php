<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $users = [];
    for ($i=1; $i <=  20 ; $i++) {
      $users[] = [
        'id' => $i,
        'name' => "user $i",
        'email' => "user$i@email.com",
        'password'=> bcrypt('123456'),
      ];
    }
    DB::table('users')->insert($users);
  }
}
