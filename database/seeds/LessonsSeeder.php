<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LessonsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $courses_lessons = [];
      for ($i=1; $i <=  5 ; $i++) {
        for ($j=1; $j <= 9 ; $j++) {
          array_push($courses_lessons, ['title' => "lesson $j", 'course_id' => $i]);
        }
      }
      DB::table('lessons')->insert($courses_lessons);
    }
}
