<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UsersCoursesReportController extends Controller
{
  public function index()
  {
    // $report_query = "SELECT UC.*, U.name AS user_name, C.name AS course_name,(SELECT COUNT(L.id)  FROM lessons AS L WHERE L.course_id = UC.course_id) AS lessons_count
    // FROM users_courses AS UC
    // LEFT JOIN users AS U ON UC.user_id = U.id
    // LEFT JOIN courses AS C ON UC.course_id = C.id";

    $view_sql_query = "CREATE OR REPLACE VIEW UsersCoursesViews AS
    SELECT UC.*, U.name AS user_name, C.name AS course_name
    FROM users_courses AS UC
    LEFT JOIN users AS U ON UC.user_id = U.id
    LEFT JOIN courses AS C ON UC.course_id = C.id";
    DB::select($view_sql_query);

    $report_query = "SELECT V.*, COUNT(L.id) AS lessons_count FROM UsersCoursesViews AS V
    LEFT JOIN lessons AS L ON L.course_id = V.course_id GROUP BY V.id";


    $subscriptions = DB::select($report_query);
    foreach ($subscriptions as $key => $subscribe) {
      $subscriptions[$key]->status = count(json_decode($subscribe->attendance))-$subscribe->lessons_count == 0? 'Passed':'Faild';
    }

    return $subscriptions;
  }
}
