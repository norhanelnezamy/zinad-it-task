<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TasksReportController extends Controller
{

  private $all_tasks;

  public function index()
  {
    $tasks = DB::table('tasks')->get()->toArray();
    $this->all_tasks = $tasks;
    $root_tasks = array_values(array_filter($tasks, function($task){
      return empty($task->parent_id);
    }));

    foreach ($root_tasks as $key => $root_task) {
      $root_tasks[$key]->childreen  = json_decode($this->getNodeChildreen($root_task));
    }
    return $root_tasks;
  }

  public function getNodeChildreen($node)
  {
    $childreen_nodes = array_values(array_filter($this->all_tasks, function($task) use($node) {
      return $task->parent_id == $node->id ;
    }));

    foreach ($childreen_nodes as $key => $child_node) {
      $childreen_nodes[$key]->childreen  = json_decode($this->getNodeChildreen($child_node));
    }
    return json_encode($childreen_nodes);
  }
}
